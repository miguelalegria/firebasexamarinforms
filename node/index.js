const {google} = require('googleapis');
        const scopes = [
            'https://www.googleapis.com/auth/firebase', // Add this scope
            'https://www.googleapis.com/auth/firebase.database',
            'https://www.googleapis.com/auth/firebase.messaging',
            'https://www.googleapis.com/auth/identitytoolkit',
            'https://www.googleapis.com/auth/userinfo.email',
        ];

function getAccessToken() {
  return new Promise(function(resolve, reject) {
    var key = require('./service-account.json');
    var jwtClient = new google.auth.JWT(
      key.client_email,
      null,
      key.private_key,
      scopes,
      null
    );
    jwtClient.authorize(function(err, tokens) {
      if (err) {
        console.log(err)
        reject(err);
        return;
      }
      console.log(tokens.access_token);
      resolve(tokens.access_token);
    });
  });
}

getAccessToken();
